

## Imported libraries
import serial
import time
import json
import Init
import gps
import Adafruit_ADXL345
import Accel
import paho.mqtt.client as mqtt
from datetime import datetime

## Global constants
LOOPDELAY = 1
LOOP = 4
###################
# MQTT
CLIENTID = 'KVP'
MQTTIMEOUT = 125
IP = '35.173.37.208'
###################
# GPS
PORTGPS = '/dev/ttyO4'
BAUDRATEGPS = 9600
TIMEOUTGPS = 1
###################
# Gyroscope
BUSID = 1
DADDR = 0x53
IDADDR = 0x00
EID = '0xe5'
XADDR = 0x32
YADDR = 0x34
ZADDR = 0x36
LC = 0.0039
###################

def on_connect(CLIENTID,flags,rc):
    print('flags - ',flags)
    print('rs = ',rc)

## Object creation 

send = mqtt.Client(client_id=CLIENTID,clean_session=False,userdata=None,
                  protocol=mqtt.MQTTv311,transport='tcp')

Init.UARTInit()
GPSerial = serial.Serial(PORTGPS,baudrate=BAUDRATEGPS,
                              timeout=TIMEOUTGPS,
                              stopbits=serial.STOPBITS_ONE,
                              parity=serial.PARITY_NONE,
                              bytesize=serial.EIGHTBITS)

Init.I2CInit()
Acc = Adafruit_ADXL345.ADXL345(address=DADDR,busnum=BUSID)

Acc.set_range(Adafruit_ADXL345.ADXL345_RANGE_4_G)
Acc.set_data_rate(Adafruit_ADXL345.ADXL345_DATARATE_25_HZ)

send.connect_async(IP,port=1883,keepalive=125)

send.loop_start()

log = 1
count = 0
while log:
    x, y, z = Acc.read()
    xAcc = Accel.GIt(x,LC)
    yAcc = Accel.GIt(y,LC)
    zAcc = Accel.GIt(z,LC)
    GGA = gps.getGGA(GPSerial)
    RMC = gps.getRMC(GPSerial)
    gpsTime = gps.getTime(RMC)
    v = gps.isValid(RMC)
    numSat = gps.Sats(GGA)
    lat = gps.getLat(GGA)
    lon = gps.getLon(GGA)
    alt = gps.getAlt(GGA)
    vel = gps.getVel(RMC)
    jsGPS = {
        'time': gpsTime,
        'isValid': v,
        'numSat': numSat,
        'lat': lat,
        'lon': lon,
        'alt': alt,
        'vel': vel
    }
    jsAcc = {
        'time': gpsTime,
        'xAcc': xAcc,
        'yAcc': yAcc,
        'zAcc': zAcc
    }
    with open('Accel.json','w') as jsonFile:
        json.dump(jsAcc,jsonFile)
    with open('GPS.json','w') as jsonFile:
        json.dump(jsGPS,jsonFile)
    print(gpsTime)
    count = count + LOOPDELAY
    if count == LOOP:
        # MQTT send here
        bGPS = json.dumps(jsGPS)
        #bGPS = bGPS.decode('UTF-8')
        bAcc = json.dumps(jsAcc)
        #bAcc = bAcc.decode('UTF-8')
        send.publish('KVP/GPSData',bGPS)
        send.publish('KVP/AccData',bAcc)
        count = 0
    time.sleep(LOOPDELAY)
