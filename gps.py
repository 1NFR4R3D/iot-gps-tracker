

## Includes

## Constants

def getGGA(GPSerial):
    GGA = GPSerial.readline()
    GGA = GGA.decode().strip().split(',')
    while GGA[0] != '$GPGGA':
        GGA = GPSerial.readline()
        GGA = GGA.decode().strip().split(',')
    return GGA

def getRMC(GPSerial):
    RMC = GPSerial.readline()
    RMC = RMC.decode().strip().split(',')
    while RMC[0] != '$GPRMC':
        RMC = GPSerial.readline()
        RMC = RMC.decode().strip().split(',')
    return RMC

def getTime(GPS):
    if GPS[0] != '$GPRMC':
        return -1
    return GPS[1]

def isValid(GPS):
    if GPS[0] != '$GPRMC':
        return -1
    if GPS[2] == 'A':
        return True
    else:
        return False

def Sats(GPS):
    if GPS[0] != '$GPGGA':
        return -1
    return GPS[7]

def getLat(GPS):
    if GPS[0] != '$GPGGA':
        return -1
    return (GPS[2] + GPS[3])

def getLon(GPS):
    if GPS[0] != '$GPGGA':
        return -1
    return (GPS[4] + GPS[5])

def getAlt(GPS):
    if GPS[0] != '$GPGGA':
        return -1
    return (GPS[9] + GPS[10])

def getVel(GPS):
    if GPS[0] != '$GPRMC':
        return -1
    return GPS[7]
