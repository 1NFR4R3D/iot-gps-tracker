

## Imported libraries
import time
import subprocess

# Validates communication with gyroscope module and initialises it
def GyroInit(i2cbus,DADDR,IDADDR,EID):
    GyroLock=True
    while GyroLock:
        a = hex(i2cbus.read_byte_data(DADDR,IDADDR))
        print(a)
        if a == EID:
            GyroLock = False
        time.sleep(1)
    print("GyroLocked!")
    i2cbus.write_byte_data(DADDR,0x2c,0x0a)
    i2cbus.write_byte_data(DADDR,0x31,0x0a)
    i2cbus.write_byte_data(DADDR,0x2d,0x08)

# Initialises the I2C bus for communication with the gyroscope
def I2CInit():
    subprocess.run(['config-pin','P9.17','i2c'])
    subprocess.run(['config-pin','P9.18','i2c'])

def UARTInit():
    subprocess.run(['config-pin','P9.11','uart'])
    subprocess.run(['config-pin','P9.13','uart'])

