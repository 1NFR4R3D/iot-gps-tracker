

## Included libraries

# Convert raw gyroscope output data (word) to 10 bit signed int
def signIt(wordData):
    intData = wordData & 1023
    if intData > 511:
        return ((1024 - intData) * (-1))
    else:
        return intData

# Convert int gyroscope data to Gs
def GIt(number,LC):
    return (number * LC)
